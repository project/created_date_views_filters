# Created Date Views Filters

This module provide the months and years dropdown filter for created date in views

For a full description of the module, visit the
[project page](https://www.drupal.org/project/created_date_views_filters).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/created_date_views_filters).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires core module views.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will provide the "Month filter: Filter" and  "Year filter: Filter" in the view.
[Year filter: Filter](https://www.drupal.org/files/project-images/year_filter.png)


## Maintainers
- Chander Bhushan - [chanderbhushan](https://www.drupal.org/u/chanderbhushan)
