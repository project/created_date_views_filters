<?php

namespace Drupal\created_date_views_filters\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filters by month of created date.
 *
 * @ViewsFilter("month_filter")
 */
class MonthFilter extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    return $this->t('Filters by month of created date.');
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'select',
      '#title' => $this->t('Month'),
      '#options' => [
        'january' => $this->t('January'),
        'february' => $this->t('February'),
        'march' => $this->t('March'),
        'april' => $this->t('April'),
        'may' => $this->t('May'),
        'june' => $this->t('June'),
        'july' => $this->t('July'),
        'august' => $this->t('August'),
        'september' => $this->t('September'),
        'october' => $this->t('October'),
        'november' => $this->t('November'),
        'december' => $this->t('December'),
      ],
      '#default_value' => $this->value,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $table = array_key_first($query->tables);

    if (!is_array($this->value)) {
      if (!empty($this->value)) {
        $month_num = date('m', strtotime($this->value));
        $query->addWhereExpression(0, "EXTRACT(MONTH FROM FROM_UNIXTIME($table.created)) = :month", [':month' => $month_num]);
      }
    }
    else {
      if (!empty($this->value[0])) {
        $month_num = date('m', strtotime($this->value[0]));
        $query->addWhereExpression(0, "EXTRACT(MONTH FROM FROM_UNIXTIME($table.created)) = :month", [':month' => $month_num]);
      }
    }
  }

}
