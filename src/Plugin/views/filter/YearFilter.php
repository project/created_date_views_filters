<?php

namespace Drupal\created_date_views_filters\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filters by year of created date.
 *
 * @ViewsFilter("year_filter")
 */
class YearFilter extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    return $this->t('Filters by year of created date.');
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $current_year = date('Y');
    $options = [];

    for ($year = date('Y') - 5; $year <= $current_year; $year++) {
      $options[$year] = $year;
    }

    $form['value'] = [
      '#type' => 'select',
      '#title' => $this->t('Year'),
      '#options' => $options,
      '#default_value' => $current_year,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $table = array_key_first($query->tables);

    if (!is_array($this->value)) {
      if (!empty($this->value)) {
        $query->addWhereExpression(0, "EXTRACT(YEAR FROM FROM_UNIXTIME($table.created)) = :year", [':year' => $this->value]);
      }
    }
    else {
      if (!empty($this->value[0])) {
        $query->addWhereExpression(0, "EXTRACT(YEAR FROM FROM_UNIXTIME($table.created)) = :year", [':year' => $this->value[0]]);
      }
    }
  }

}
